const path = require('node:path');
const fs = require('node:fs/promises');
const {ESLint} = require('eslint');
const glob = require('./glob.js');

const DEFAULT_IGNORES = [
	'**/node_modules/**',
	'**/bower_components/**',
	'{tmp,temp}/**',
	'**/*.min.js',
	'vendor/**',
	'dist/**'
];

const DEFAULT_EXTENSIONS = ['js', 'mjs', 'cjs'];
const DEFAULT_FORMATTER = 'pretty';
const DEFAULT_CONFIG = {
	cwd: '.',
	fix: false,
	glob: true,
	usePackageJson: true,
	errorOnUnmatchedPattern: false,
	filterOverrides: true,
	cache: 'node_modules/.wxo-cache',
	extensions: DEFAULT_EXTENSIONS,
	ignores: DEFAULT_IGNORES,
	format: DEFAULT_FORMATTER,
	envs: ['node'],
	extends: [require.resolve('../presets/js.js')],
	globals: [],
	rules: {},
	overrides: []
};

async function fileExists(filePath) {
	try {
		await fs.access(filePath);
		return true;
	} catch {
		return false;
	}
}

async function getFormatter(eslint, format) {
	if(typeof(format) === 'function') {
		return format;
	}

	format ||= 'pretty';

	if(format === 'pretty') {
		const pretty = await import('eslint-formatter-pretty');
		return pretty.default;
	}

	return eslint.loadFormatter(format);
}

function mergeOptionsOne(base, overrides) {
	base ||= DEFAULT_CONFIG;

	if(!overrides) {
		return base;
	}

	const merged = {};

	for(const [key, defaultValue] of Object.entries(DEFAULT_CONFIG)) {
		if(('reset' in overrides) && (overrides.reset === 'all' || key === overrides.reset || overrides.reset.includes(key))) {
			merged[key] = Array.isArray(defaultValue) ? [] : defaultValue;
		} else {
			merged[key] = key in base ? base[key] : defaultValue;
		}

		if(!(key in overrides)) {
			continue;
		}

		if(typeof(defaultValue) === 'boolean') {
			merged[key] = Boolean(overrides[key]);
			continue;
		}

		if(typeof(defaultValue) === 'string') {
			merged[key] = overrides[key] ? String(overrides[key]) : null;
			continue;
		}

		if(!overrides[key]) {
			continue;
		}

		if(Array.isArray(defaultValue)) {
			merged[key] = [...merged[key]];
			if(Array.isArray(overrides[key])) {
				merged[key].push(...overrides[key]);
			} else {
				merged[key].push(overrides[key]);
			}
		} else {
			merged[key] = {
				...merged[key],
				...overrides[key]
			};
		}
	}

	return merged;
}

function mergeOptions(config, ...args) {
	for(const overrides of args) {
		config = mergeOptionsOne(config, overrides);
	}

	return config;
}

async function resolveEslintConfig(patterns, options) {
	let files = patterns;

	if(options.usePackageJson) {
		const packagePath = path.resolve(options.cwd, './package.json');
		if(await fileExists(packagePath)) {
			const pkg = require(packagePath);
			options = mergeOptionsOne(options, pkg && pkg.wxo);
		}
	}

	if(options.glob) {
		files = await glob.files(patterns, options);
	}

	const overrides = options.filterOverrides ? glob.filterOverrides(files, options.overrides) : options.overrides;

	return {
		files,
		useEslintrc: false,
		globInputPaths: false,
		ignore: false,
		resolvePluginsRelativeTo: __dirname,
		fix: options.fix,
		cwd: options.cwd,
		errorOnUnmatchedPattern: options.errorOnUnmatchedPattern,
		extensions: options.extensions,
		cache: Boolean(options.cache),
		cacheLocation: options.cache,
		baseConfig: {
			overrides,
			ignorePatterns: options.ignorePatterns,
			plugins: options.plugins,
			extends: options.extends,
			rules: options.rules,
			env: Object.fromEntries(options.envs.map(key => ([key, true]))),
			globals: Object.fromEntries(options.globals.map(key => ([key, 'readonly'])))
		}
	};
}

async function lintFiles(patterns, options = DEFAULT_CONFIG) {
	const {format, files, ...config} = await resolveEslintConfig(patterns, options);

	const eslint = new ESLint(config);
	const results = await eslint.lintFiles(files);

	if(config.fix) {
		await ESLint.outputFixes(results);
	}

	const formatter = await getFormatter(eslint, format);
	const errorCount = results.reduce((count, result) => count + result.errorCount, 0);

	return {
		errorCount,
		success: errorCount === 0,
		results,
		output: await formatter(results)
	};
}

module.exports = {
	DEFAULT_IGNORES,
	DEFAULT_CONFIG,
	DEFAULT_FORMATTER,
	DEFAULT_EXTENSIONS,
	mergeOptions,
	resolveEslintConfig,
	lintFiles
};
