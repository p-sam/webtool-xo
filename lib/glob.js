const path = require('node:path');
const minimatch = require('minimatch');
const fastGlob = require('fast-glob');

async function globFiles(patterns, {ignores, extensions, cwd}) {
	if(extensions && !Array.isArray(extensions)) {
		extensions = [extensions];
	}

	if(!patterns || patterns.length === 0) {
		patterns = extensions && extensions.length > 0 ? [`**/*.{${extensions.join(',')}}`] : ['**/*'];
	}

	const files = await fastGlob(patterns, {ignore: ignores, cwd});

	if(!extensions || extensions.length === 0) {
		return files;
	}

	return files.filter(file => extensions.includes(path.extname(file).slice(1)));
}

function compileOverrideMatcher(override) {
	if(!override || !override.files || override.files.length === 0) {
		return null;
	}

	const matchers = override.files.map(pattern => new minimatch.Minimatch(pattern, {dot: true, matchBase: true}));

	return file => matchers.find(m => m.match(file));
}

function globFilterOverrides(files, overrides = []) {
	if(!overrides || overrides.length === 0) {
		return [];
	}

	const filteredOverrides = [];
	const remainingOverrides = new Map(overrides.map(override => ([override, compileOverrideMatcher(override)])));

	for(const file of files) {
		for(const [override, matcher] of remainingOverrides) {
			if(!matcher || matcher(file)) {
				filteredOverrides.push(override);
				remainingOverrides.delete(override);
			}
		}

		if(remainingOverrides.size === 0) {
			break;
		}
	}

	return filteredOverrides;
}

module.exports = {files: globFiles, filterOverrides: globFilterOverrides};
