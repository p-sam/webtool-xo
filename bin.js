#!/usr/bin/env node

const minimist = require('minimist');
const xo = require('./lib/linter.js');

async function main() {
	const argv = minimist(process.argv.slice(2));
	const input = argv._;
	const config = xo.mergeOptions({cwd: process.cwd()}, argv);

	if(argv.inspect) {
		process.stdout.write(JSON.stringify(
			argv.inspect === 'eslint' ? await xo.resolveEslintConfig(input, config) : config,
			null, 4
		));
		process.stdout.write('\n');

		return 0;
	}

	const result = await xo.lintFiles(input, config);
	process.stdout.write(result.output);

	return result.success ? 0 : 1;
}

main()
	.then(rc => {
		process.exitCode = rc;
	})
	.catch(error => {
		console.error(error);
		process.exitCode = 1;
	});
