module.exports = {
	extends: [
		'plugin:unicorn/recommended',
		'plugin:no-use-extend-native/recommended',
		'plugin:promise/recommended',
		'plugin:import/errors',
		'plugin:import/electron',
		'plugin:node/recommended',
		'plugin:eslint-comments/recommended'
	],
	rules: {
		// XO: The character class sorting is a bit buggy at the moment.
		'unicorn/better-regex': ['error', {sortCharacterClasses: false}],

		// XO: Rules deemed obnoxious,
		'unicorn/prefer-top-level-await': 'off',
		'unicorn/prefer-module': 'off',
		'unicorn/consistent-destructuring': 'off',
		'unicorn/no-null': 'off',
		'unicorn/prevent-abbreviations': 'off',
		'unicorn/numeric-separators-style': 'off',
		'unicorn/no-array-callback-reference': 'off',
		'unicorn/switch-case-braces': 'off',

		// XO: We only enforce it for single-line statements to not be too opinionated.
		'unicorn/prefer-ternary': ['error', 'only-single-line'],

		// XO: Buggy/immature rules
		'unicorn/consistent-function-scoping': 'off',
		'unicorn/no-useless-undefined': 'off',
		'function-call-argument-newline': 'off',

		// Promise: recommended overrides
		'promise/always-return': 'off',
		'promise/catch-or-return': 'off',
		'promise/no-promise-in-callback': 'off',
		'promise/no-callback-in-promise': 'off',
		'promise/no-return-in-finally': 'off',
		'promise/valid-params': 'error',

		// Import: recommended overrides
		'import/extensions': ['error', 'always', {ignorePackages: true}],
		'import/namespace': ['error', {allowComputed: true}],
		'import/no-absolute-path': 'error',
		'import/no-named-default': 'error',
		'import/no-self-import': 'error',
		'import/no-cycle': ['error', {ignoreExternal: true}],
		'import/no-useless-path-segments': 'error',
		'import/newline-after-import': 'error',
		'import/no-amd': 'error',
		'import/no-duplicates': 'error',
		'import/no-named-as-default-member': 'error',
		'import/no-named-as-default': 'error',
		'import/order': 'error',

		// Node: recommended overrides
		'node/no-unpublished-bin': 'error',
		'node/file-extension-in-import': 'off',
		'node/no-new-require': 'error',
		'node/no-path-concat': 'error',
		'node/no-unpublished-import': 'off',
		'node/no-unpublished-require': 'off',
		'node/no-extraneous-import': 'off',
		'node/no-extraneous-require': 'off',
		'node/no-missing-import': 'off',
		'node/no-missing-require': 'off',
		'node/no-unsupported-features/es-syntax': 'off',
		'node/no-unsupported-features/es-builtins': 'off',
		'node/shebang': 'off',

		'node/prefer-global/buffer': ['error', 'always'],
		'node/prefer-global/console': ['error', 'always'],
		'node/prefer-global/process': ['error', 'always'],
		'node/prefer-global/text-decoder': ['error', 'always'],
		'node/prefer-global/text-encoder': ['error', 'always'],
		'node/prefer-global/url-search-params': ['error', 'always'],
		'node/prefer-global/url': ['error', 'always'],
		'node/prefer-promises/dns': 'error',
		'node/prefer-promises/fs': 'error',

		// eslint-comments: recommended overrides
		'eslint-comments/disable-enable-pair': ['error', {allowWholeFile: true}],
		'eslint-comments/no-unlimited-disable': 'off'
	}
};
