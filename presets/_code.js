const CONTROL_FLOW_KEYWORDS = ['if', 'for', 'while', 'switch'];
const UNARY_OPS_KEYWORDS = ['typeof', 'void'];

module.exports = {
	rules: {
		'keyword-spacing': ['error', {
			overrides: Object.fromEntries(CONTROL_FLOW_KEYWORDS.map(k => [k, {after: false}]))
		}],
		'space-unary-ops': ['error', {
			overrides: Object.fromEntries(UNARY_OPS_KEYWORDS.map(k => [k, false]))
		}],
		'comma-dangle': ['error', 'never']
	}
};
