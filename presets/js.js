function mergeConfig(base, overrides) {
	const merged = {...base, ...overrides};

	for(const k of ['parserOptions', 'env', 'rules', 'globals', 'settings']) {
		const baseObject = k in base ? base[k] : {};
		merged[k] = {...baseObject, ...overrides[k]};
	}

	return merged;
}

function mergeConfigs() {
	let config = require('eslint-config-xo');

	config = mergeConfig(config, require('./_plugins.js'));
	config = mergeConfig(config, require('./_globals.js'));
	config = mergeConfig(config, require('./_code.js'));

	return config;
}

module.exports = mergeConfigs();
