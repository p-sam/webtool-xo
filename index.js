module.exports = {
	glob: require('./lib/glob.js'),
	...require('./lib/linter.js')
};
